var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var mysql = require('mysql');
var apiai = require('apiai');
var app_dia = apiai('1fe2c857ed914d439a2e902351b7785d');
var clientes = {};
var operadores = {};

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "chat_nuevaeps"
});

con.connect((err)=>{
    if(!err){
        console.log("conectado_correctamente")
    }else{
        console.log("DB connection failed  Error : "+err);
    }
});



app.get('/cliente', function(req, res){
    res.sendFile(__dirname + '/cliente.html');
});


app.get('/operador', function(req, res){
    res.sendFile(__dirname + '/operador.html');
});


app.get('/cliente_2', function(req, res){
    res.sendFile(__dirname + '/cliente_bk.html');
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});

var getRes = function(query) {
    var request = app_dia.textRequest(query, {
        sessionId: '<unique session id>'
    });
  const responseFromAPI = new Promise(
          function (resolve, reject) {
  request.on('error', function(error) {
      reject(error);
  });
  request.on('response', function(response) {
    //resolve(response.result.fulfillment.speech);
    resolve(response);
  });
  });
  request.end();
  return responseFromAPI;
};

io.sockets.on('connection',function(socket){
    
    socket.on('new client', function(data,callback){
            callback(true);
            var conexion_socket = socket.conn;
            socket.nickname = data;
            socket.id_socket_c = conexion_socket.id;
            clientes[socket.id_socket_c] = socket;
            updateNicknames();
            var json = {mensaje:"hola",cliente:conexion_socket.id};
            getRes(json.mensaje).then(function(res){
                var mensaje_resultado = res.result.fulfillment.speech;
                var action = res.result.action;
                var ob = {mensaje_r:mensaje_resultado,cliente:json.cliente,accion:action};
                var socketsillo = clientes[json.cliente];
                //console.log("se almacena la respuesta enviada por dialog flow"+json.cliente);
                socketsillo.emit('nuevo mensajediag',ob);
            });
    });


    socket.on('new operator', function(data,callback){

        console.log(data);


        if(data in operadores){
            callback(false);
        }else{
            callback(true);
            var conexion_socket = socket.conn;
            socket.nickname = data;
            socket.id_socket_c = conexion_socket.id;
            //console.log(socket.id_socket_c);
            operadores[socket.id_socket_c] = socket;
            //console.log(operadores);
            updateNicknames();
        }
    });

    function updateNicknames(){
       // console.log(Object.keys(operadores));

        io.sockets.emit('usernames_client',Object.keys(clientes));
        io.sockets.emit('usernames_operadores',Object.keys(operadores));
    }

     
    socket.on("disconnect",function(data){
        if(!socket.id_socket_c) return;
        delete clientes[socket.id_socket_c];
        delete operadores[socket.id_socket_c];
        updateNicknames();
    });

    socket.on('send mcliente',function(data){

       

        var json = JSON.parse(data);
        console.log(json.dummy);
        var socketsillo = operadores[json.operador];
        if(json.dummy == 'on'){

            var sql_explora = "SELECT mensaje,usuario FROM chat WHERE cliente = '"+json.cliente+"' and usuario IN ('cliente','bot')";
            con.query(sql_explora,function(erra,resula){
                if(!erra){
                    json.datos_bk = resula;
                    //console.log(json.datos_bk);
                    socketsillo.emit('nuevo mensajecli',json);
                }else{
                    console.log(erra);
                }
            });

        }else if(json.dummy == "off"){

            var sql = "INSERT INTO chat(mensaje,usuario,cliente) VALUES ('"+json.mensaje+"','cliente','"+json.cliente+"')";
            con.query(sql, function (errc, result) {
                if(!errc){

                var sql_explora = "SELECT mensaje,usuario FROM chat WHERE cliente = '"+json.cliente+"'";
                con.query(sql_explora,function(erra,resula){
                    if(!erra){
                        json.datos_bk = resula;
                        //console.log(json.datos_bk);
                        socketsillo.emit('nuevo mensajecli',json);
                    }else{
                        console.log(erra);
                    }
                });


                    /*json.datos_bk = "N";
                    socketsillo.emit('nuevo mensajecli',json);*/
                }else{
                    console.log(errc);
                }
            });

        }
    });

    socket.on('send mcoperador',function(data){
        var json = JSON.parse(data);
        var socket_operador = operadores[json.operador];
        var socketsillo = clientes[json.cliente];

        var sql = "INSERT INTO chat(mensaje,usuario,cliente) VALUES ('"+json.mensaje+"','"+socket_operador.nickname+"','"+json.cliente+"')";
        con.query(sql, function (errc, result) {
            if(!errc){
                socketsillo.emit('nuevo mensajeope',json);
            }else{
                console.log(errc);
            }
        });

        
    });

    socket.on('send mcdialogflow',function(data){

      
        var json = JSON.parse(data);
       // console.log("se almacena la data envio hacia dialog flow cliente :"+json.cliente);

        getRes(json.mensaje).then(function(res){
            var mensaje_enviado = res.result.resolvedQuery
            var mensaje_resultado = res.result.fulfillment.speech;
            var action = res.result.action;
            var ob = {mensaje_r:mensaje_resultado,cliente:json.cliente,accion:action,mensaje_cliente:mensaje_enviado};
            var socketsillo = clientes[json.cliente];
            //console.log("se almacena la respuesta enviada por dialog flow"+json.cliente);
                
            var sql = "INSERT INTO chat(mensaje,usuario,cliente) VALUES ('"+ob.mensaje_cliente+"','cliente','"+ob.cliente+"')";
            con.query(sql, function (errc, result) {
                if(!errc){
                    var sql_dialog = "INSERT INTO chat(mensaje,usuario,cliente) VALUES ('"+mensaje_resultado+"','bot','"+ob.cliente+"')";
                    con.query(sql_dialog, function (err_d, result_d) {
                        if(!err_d){
                            socketsillo.emit('nuevo mensajediag',ob);
                        }else{
                            console.log(err_d);        
                        }
                    });                             
                }else{
                    console.log(errc);
                }
            });
               
           
        });

    });


});