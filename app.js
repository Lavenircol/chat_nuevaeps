var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var users = {};

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});


http.listen(3000, function(){
    console.log('listening on *:3000');
});

io.sockets.on('connection',function(socket){

    socket.on('new user', function(data,callback){
        if(data in users){
            callback(false);
        }else{
            callback(true);
            socket.nickname = data;
            users[socket.nickname] = socket;
            updateNicknames();
        }
    });

    function updateNicknames(){
        io.sockets.emit('usernames',Object.keys(users));
    }

    socket.on('send message',function(data,callback){
        var msg = data.trim();
        if(msg.substr(0,3) === '/w '){
            msg = msg.substr(3);
            var ind = msg.indexOf(' ');
            if(ind !== -1){
                var name = msg.substring(0,ind);
                var msg = msg.substring(ind + 1);
                if(name in users){
                    users[name].emit('whisper',{mgs:msg,nick:socket.nickname});
                    console.log("holaaaaaa");
                }else{
                    callback("error usuario no existe");
                }
                
            }else{
                callback('error ingrese un mensaje');
            }
        }else{
        io.sockets.emit('new message',{mgs:msg,nick:socket.nickname});
        }
        //socket.broadcast.emit('new message',data);
    });

    socket.on("disconnect",function(data){
        if(!socket.nickname) return;
        delete users[socket.nickname];
        updateNicknames();
    });

});

//new user


  